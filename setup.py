import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="picamera_gui-antortjim", # Replace with your own username
    version="0.0.0",
    author="Antonio Ortega",
    author_email="ntoniohu@gmail.com",
    description="A web based picamera GUI",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/antortjim/picamera_gui",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3.7",
        "License :: OSI Approved :: GNU General Public License v2 (GPLv2)",
        "Operating System :: POSIX :: Linux",
        "Programming Language :: JavaScript"
    ],
    python_requires='>=3.7.4',
)

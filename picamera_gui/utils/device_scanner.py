import logging
import threading
import urllib.request, urllib.error, urllib.parse
import os
import datetime
import json
import time
import logging
import traceback
import threading
from functools import wraps
import socket
from zeroconf import ServiceBrowser, Zeroconf
import json

from picamera_gui.utils.configuration import PiCameraConfiguration
from picamera_gui.ethoscope.node_src.ethoscope_node.utils.device_scanner import DeviceScanner, Ethoscope
from picamera_gui.utils.variables import ParameterSet
import picamera_gui.utils.helpers as helpers

class ProxyConf:

    def set_proxy_conf(self, uri, port):
        self._proxy = uri
        self._proxy_port = port


class Picamera(Ethoscope, ProxyConf):

    _port = 19000  
    
    def __init__(self, *args, **kwargs):
        '''
        Picamera class
        '''

        self._remote_pages.update({"picamera": "picamera_data"})

        if "port" in kwargs.keys():
            pass
        else:
            kwargs["port"] = self._port

        super().__init__(*args, **kwargs)

    def update(self, post_data):
        
        params = json.loads(post_data.decode())
        param_set = ParameterSet(params, default=False)

        data = param_set.urlencode()
        logging.warning("Sending data to raspberrypi:update()")
        logging.warning(data)
        post_url = "http://%s:%i/%s/%s" % (self._ip, self._port, self._remote_pages['update'], self._id)
        return self._get_json(post_url, timeout=5, post_data = data)

    def picamera(self):
        '''
        Retrieves private machine info from the ethoscope
        This is used to check if the ethoscope is a new installation
        '''
        url = "http://%s:%i/%s/%s" % (self._ip, self._port, self._remote_pages['picamera'], self._id)
        out = self._get_json(url)
        return out

    def _make_backup_path(self):
        return {"backup_path": None}


class PiCameraScanner(DeviceScanner, ProxyConf):
    """
    PiCameraGUI specific scanner
    """
    _suffix = ".local" 
    _service_type = "_picamera._tcp.local." 
    _device_type = "picamera"
    results_dir = None


    def __init__(self, *args, **kwargs):
        if "deviceClass" not in kwargs.keys(): kwargs["deviceClass"] = Picamera
        super().__init__(*args, **kwargs)

    def start(self):
        super().start()
 



    def add(self, ip, name=None, device_id=None):
        """
        Manually add a device to the list
        """

        device = self._Device(ip, self.device_refresh_period, proxy = self._proxy)
        if name: device.zeroconf_name = name

        device.start()

        if not device_id: device_id = device.id()

        self.devices.append(device)

        logging.info("New %s manually added with name = %s, id = %s at IP = %s" % (self._device_type, name, device.id(), ip))

if __name__ == "__main__":
    import argparse
    ap = argparse.ArgumentParser()
    ap.add_argument("--ip", type = str, required=True)
    args = ap.parse_args()
    args = vars(args)
    picamera_gui = Picamera(args["ip"], 5, proxy=None)

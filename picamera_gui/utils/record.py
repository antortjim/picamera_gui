from os import path
# from threading import Thread
import traceback
import logging
import time
import os
import tempfile
import shutil
import multiprocessing
import glob
import datetime
from PIL import Image, ImageDraw, ImageFont
import socketserver
import os.path

#For streaming
from http.server import BaseHTTPRequestHandler, HTTPServer
import io
from picamera_gui.utils.configuration import PiCameraConfiguration
CFG = PiCameraConfiguration()
FONTS_DIR = CFG.content["fonts"]["dir"]

class PiCamGUIHandler(BaseHTTPRequestHandler):
    '''
    The Handler to the Camera Stream interrogates the camera when it runs
    '''
    def do_GET(self):
        if self.path.endswith('.mjpg'):
            self.send_response(200)
            self.send_header('Content-type','multipart/x-mixed-replace; boundary=--jpgboundary')
            self.end_headers()
            stream=io.BytesIO()

            try:
              time.time()
              for _ in self.server.camera.capture_continuous(stream,'jpeg',use_video_port=True):

                self.wfile.write(b"--jpgboundary")
                self.send_header('Content-type','image/jpeg')
                self.send_header('Content-length',len(stream.getvalue()))
                self.end_headers()

                overlay =  self.overlay(stream = stream, resolution = self.server.camera.resolution)
                if overlay:
                    pass
                else:
                    self.wfile.write(stream.getvalue())

                stream.seek(0)
                stream.truncate()
                time.sleep(.1)
                if self.server.camera.closed:
                    return

            except KeyboardInterrupt:
                pass
            return
        else:
            self.send_response(200)
            self.send_header('Content-type','text/html')
            self.end_headers()
            self.wfile.write(b"""<html><head></head><body>
              <img src="/cam.mjpg"/>
            </body></html>""")
            return

    def overlay(self, stream, resolution):
        return False


class Overlay:

    # declare wfile type so lint does not warn wfile does not exist
    wfile:socketserver._SocketWriter
    fonts_dir = FONTS_DIR 
    logging.warning(fonts_dir)
    font_path = os.path.join(fonts_dir, "FreeMono.ttf")

    def overlay(self, stream: io.BytesIO, resolution: tuple):
        # based on
        # https://www.raspberrypi.org/forums/viewtopic.php?t=189003

        w, h = resolution
        x = w / 3
        y = h / 15
        pos = (x,y)
        current_image = Image.open(stream).convert('RGB')
        imgdraw = ImageDraw.Draw(current_image)
        now = datetime.datetime.utcnow().strftime('%Y-%m-%d %H:%M:%S.%f')[:-3]
        font_size = 40
        font = ImageFont.truetype(self.font_path, font_size)

        imgdraw.text(pos, now, fill = (255, 255, 255, 128), font=font)
        current_image.save(self.wfile, 'JPEG')
        return True


class CamHandlerAnnotate(Overlay, PiCamGUIHandler):
    pass

import urllib.parse

#def get_machine_id(path = "/etc/machine-id"):
#
#    with open(path, "r") as fh:
#        info = fh.readline().rstrip()
#    return info

def urlencode(obj, encoding='utf-8'):
    
    result = urllib.parse.urlencode(obj)
    if encoding is not None:
        result = result.encode(encoding)
    return result


def urldecode(obj, encoding='utf-8'):
    if encoding is not None:
        result = obj.decode(encoding)
    else:
        result = obj

    print(result)
    result = urllib.parse.parse_qs(result)
    return result

if __name__ == "__main__":
    post_data = b'{"awb_gains":"1.54297,1.91406","awb_mode":"auto","exposure_mode":"auto","shutter_speed":0,"exposure_speed":62998,"exposure_compensation":0,"iso":0,"brightness":50,"rotation":0,"sharpness":0,"contrast":0,"zoom":"0.0,0.0,1.0,1.0"}'
    params = urldecode(post_data)
    print(params)


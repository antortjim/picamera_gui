# picamera_gui


picamera_gui is a python/angularjs application that provides a GUI to control the settings of a RaspberryPi camera connected to a RPi from a remote computer (**server/node**). It shows a live feed that the user can interact with by changing the camera parameters. These parameters are some of those specified in https://picamera.readthedocs.io/en/release-1.13/api_camera.html (see section below). They can be changed and the result of the change observed in a closed loop. The feed writes the UTC date and time as an overlay.

It was designed to help debug tracking errors with the ethoscopes and help find a good combination of paramters in the picamera.

The software is based on https://github.com/gilestrolab/ethoscope/ and makes use of it as a dependency. Thus, picamera_gui **inherits all the dependencies from ethoscope and ethoscope_node** the two core modules available in this repo under `src/`and `node_src/`respectively .

# Requirements

**RPi OS**: Any Linux distribution, installation automated and tested in **ArchLinux ARM** only.
**Node OS**: Any Linux distribution, tested in **Ubuntu 18.04** only.
Python >= 3.7.4
Python < 3.8
picamera >= 1.13 (ony in RPi)
**Internet access on both Node and RPi**
**sudo access on both Node and RPi**
**ethoscope_node dependencies (only in Node)**
**ethoscope dependencies (only in RPi)**
Please see https://github.com/gilestrolab/ethoscope/ to see how to install this so




You need to have the version of ethoscope and ethoscope_node hosted in the picamera_gui branch available in https://gitlab.com/antortjim/ethoscope
This is so because picamera_gui makes use of and extends the functionality in the ethoscope/web_utils/record.py and ethoscope_node/utils/device_scanner.py files:

 * Implements a PiCameraScanner analogous to the EthoscopeScanner.
 * Implements a special CamHandler with support for overlay.
 * Support for a proxy server given by the file  `/etc/picamera_gui.conf` so the remote computer does not need to be in the same network as the RPi.
    


# Installation

**NB** This assumes you have sudo permission and internet access on both the node and the RPis


## Python version
Check you are running Python >= **3.7.4**. Python >= 3.8 is not supported.

`python --version`

## Picamera version

Check picamera is at version **1.13** 

In Python

```
# from https://picamera.readthedocs.io/en/release-1.13/faq.html#how-can-i-tell-what-version-of-picamera-i-have-installed
>>> from pkg_resources import require
>>> require('picamera')
[picamera 1.2 (/usr/local/lib/python2.7/dist-packages)]
>>> require('picamera')[0].version
'1.13'
```

## Download picamera_gui

Clone this repository and leave the master branch checked out. You must clone to `/opt/` or at least make a softlink in `/opt` pointing to the place where you cloned.

`ln -s CLONE_PATH /opt/picamera_gui`

## Install dependencies

**RaspberryPi**
`sudo scripts/install_device.sh`
**Node**
For an installation in the checked out conda environment
`sudo scripts/install_node.sh -r $CONDA_PREFIX`
For an installation in the root python
`sudo scripts/install_node.sh -r /usr`


These scripts automate the installation of python and system dependencies outside of the ethoscope ones (Pillow module in python and ttf-freefont package), create the systemd services and start them, and add the required folders to the python path.
They should work in an Ubuntu node and RPis running ArchLinux ARM. Customize it to fit your needs if this does not match your system



# Execution

The module runs automatically thanks to the systemd services. However, you can disable them and instead run manually:

In your remote RaspberryPi, execute:

```
cd /opt/picamera_gui/scripts
python picamera_server --port 19000 --debug
```

In your node server, execute:

```
cd /opt/picamera_gui/scripts
python picamera_server --port 10000 --debug
```

# The GUI

The GUI is accessible from the node/server at localhost:10000. You should get something like:

![](static/img/home.png)

If you click on the link available in the name of the RaspberryPI (ETHOSCOPE_007 in this case), you get the next window:

![](static/img/picamera_stopped.png)

You can click on **Start PiCamera** . In a few seconds max you should get a live feed from the RaspberryPi. At the moment, there is no way to know whether the request went through other than looking at the logs of the terminal for now.

![](static/img/picamera_running.png)

The camera parameters that can be controlled are located beneath the live feed

![](static/img/picamera_controls.png)

You can change them and observe the reaction in the live feed.

**NB** Some parameters have a dependency on other parameters. Changing them can have no effect or an effect different from the expected. For instance, changing the awb_gains has no effect unless awb_mode is set to off, and shutter_speed cannot be changed unless exposure_mode is set to off. exposure_speed is just a read-only version of shutter_speed that likewise reflects the exposure time but only when exposure_mode is not off. If it is off, the exposure time is controlled with shutter_speed. More information https://picamera.readthedocs.io/en/release-1.13/api_camera.html

**NB** At the moment, the communication between the RPi and the node server is buggy, and sometimes a change is not propagated to the RPi or reverted a few seconds after entering it. But it works 80% of the time. If it did not work, just try again! ;)

![](static/img/picamera_gui.gif)

# Supported OS

picamera_gui was developed based on the ethoscope project https://gilestrolab.github.io/ethoscope/  and it's at an early stage of development.

**RaspberryPi**: Arch Linux Arm (armvl7). Raspbian should work too possibly with some minor adjustments.
**Server**: Ubuntu 18.04. Arch Linux or any other GNU/Linux distribution should work as well.



# Supported parameters

* awb_mode
* awb_gains
* brightness
* contrast
* exposure_mode
* exposure_speed
* iso
* rotation
* sharpness
* shutter_speed


# TODO

* Improve usability by adding more information in the GUI about the state of the camera and a spinner or some reporter of progress.

* Fix bug preventing input from being propagated 100% correctly.

* Extend to support more attributes, specially framerate and resolution, which can only be set up upon initialization. This could be done with a modal available upon clicking on "Start Tracking".

* The camera does not stop even if the `camera.close()` method is called. This is a bug that makes pressing it impossible to stop the camera via the GUI. The pause button is a dummy.

* Fix minor GUI errors, like Invalid date

#!/bin/bash

# Add picamera_gui to the Python path

# ref: https://askubuntu.com/a/30157/8698
if ! [ $(id -u) = 0 ]; then
   echo "The script need to be run as root." >&2
   exit 1
fi

if [ $SUDO_USER ]; then
    real_user=$SUDO_USER
else
    real_user=$(whoami)
fi


install_node() {

	ROOT=$1
	sudo -u $real_user echo "Installing to $ROOT"
        sudo -u $real_user echo "/opt/picamera_gui/" >> $ROOT/lib/python3.7/site-packages/easy-install.pth
        
        # Create the systemd service
	sed  "s|"'$ROOT'"|$ROOT|" systemd/picamera_gui.service > /etc/systemd/system/picamera_gui.service
        systemctl daemon-reload
        systemctl start picamera_gui
        systemctl enable picamera_gui
        
        # Make sure the ethoscope submodule is checked out
        sudo -u $real_user git submodule init
        sudo -u $real_user git submodule update
        sudo -u $real_user git submodule foreach git checkout picamera_gui

	# Place default config file
        sudo -u $real_user picamera_gui.conf /etc/picamera_gui.conf
}


rflag=false

usage () { echo "How to use"; }

options='r:h'
while getopts $options option
do
    case "$option" in
        r  ) rflag=true; install_node $OPTARG;;
        h  ) usage; exit;;
        \? ) echo "Unknown option: -$OPTARG" >&2; exit 1;;
        :  ) echo "Missing option argument for -$OPTARG" >&2; exit 1;;
        *  ) echo "Unimplemented option: -$OPTARG" >&2; exit 1;;
    esac
done

if ((OPTIND == 1))
then
    echo "No options specified"
fi

shift $((OPTIND - 1))


#if (($# == 0))
#then
#    echo "No positional arguments specified"
#fi

if ! $rflag && [[ -d $1 ]]
then
    usage
    exit 1
fi



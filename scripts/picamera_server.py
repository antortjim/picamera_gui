__author__ = 'luis'

import logging
import traceback
from optparse import OptionParser
import subprocess
import json
import datetime
import os
import glob
import time
import sys
import io
import shutil
import fractions
import bottle
import socket
import netifaces
import urllib.request, urllib.error, urllib.parse
from picamera_gui.utils.variables import ParameterSet
import picamera_gui.utils.helpers as helpers
from picamera_gui.utils.configuration import PiCameraConfiguration

if __name__ == '__main__':

    

    api = bottle.Bottle()
    param_set = ParameterSet({}, default=True)
    cache_set = ParameterSet({}, default=False)
    logging.warning(param_set)
    STATIC_DIR = "../static"


    parser = OptionParser()
    parser.add_option("-r", "--run", dest="run", default=False, help="Runs tracking directly", action="store_true")
    parser.add_option("-s", "--stop-after-run", dest="stop_after_run", default=False, help="When -r, stops immediately after. otherwise, server waits", action="store_true")
    parser.add_option("-v", "--record-video", dest="record_video", default=False, help="Records video instead of tracking", action="store_true")
    parser.add_option("-j", "--json", dest="json", default=None, help="A JSON config file")
    parser.add_option("-p", "--port", dest="port", default = 19000, type=int, help="port")
    parser.add_option("-n", "--node", dest="node", default="node", help="The hostname of the computer running the node")
    parser.add_option("-D", "--debug", dest="debug", default=False, help="Shows all logging messages", action="store_true")
    parser.add_option("--proxy", dest="proxy", help="Proxy address i.e. gbw-d-l0001")
    parser.add_option("--proxy_port", dest="proxy_port", type=int, help="Proxy port i.e. 3128")
    
    (options, args) = parser.parse_args()
    option_dict = vars(options)


    PORT = option_dict["port"]
    logging.warning(f"Running on port {PORT}")
    DEBUG = option_dict["debug"]
    NODE = option_dict["node"]

class WrongMachineID(Exception):
    pass

def error_decorator(func):
    """
    A simple decorator to return an error dict so we can display it the ui
    """
    def func_wrapper(*args, **kwargs):
        try:
            return func(*args, **kwargs)
        except Exception:
            logging.error(traceback.format_exc())
            return {'error': traceback.format_exc()}
    return func_wrapper

def warning_decorator(func):
    """
    A simple decorator to return an error dict so we can display it in the webUI
    Less verbose than error
    """
    def func_wrapper(*args, **kwargs):
        try:
            return func(*args, **kwargs)
        except Exception as e:
            logging.error(traceback.format_exc())
            return {'error': str(e)}
    return func_wrapper

@api.hook('after_request')
def enable_cors():
    """
    You need to add some headers to each request.
    Don't use the wildcard '*' for Access-Control-Allow-Origin in production.
    """
    #bottle.response.headers['Access-Control-Allow-Origin'] = 'http://localhost:8888'
    bottle.response.headers['Access-Control-Allow-Origin'] = '*' # Allowing CORS in development
    bottle.response.headers['Access-Control-Allow-Methods'] = 'PUT, GET, POST, DELETE, OPTIONS'
    bottle.response.headers['Access-Control-Allow-Headers'] = 'Origin, Accept, Content-Type, X-Requested-With, X-CSRF-Token'


#######################
## PiCamera API
#######################

## Both
########################

@api.get('/node/proxy')
def get_proxy():

    global device_scanner

    return {"proxy": device_scanner._proxy, "proxy_port": device_scanner._proxy_port, "proxy_remote": device_scanner._remote_port}

@api.get('/info')
def get_info():
    return {"status": "Success"}


# region only-node

@api.get('/favicon.ico')
def get_favicon():
    return server_static(STATIC_DIR+'/img/favicon.ico')

@api.route('/')
def index():
    return bottle.static_file('index.html', root=STATIC_DIR)

#region Node-GET


@api.get('/picamera/<id>')
def redirection_to_ethoscope(id):
    return bottle.redirect('/#/picamera/'+id)


@api.get('/node/<req>')
@error_decorator
def node_info(req):#, device):
    if req == 'info':

        with os.popen('df %s -h' % RESULTS_DIR) as df:
            disk_free = df.read()
        
        disk_usage = RESULTS_DIR+" Not Found on disk"

        CARDS = {}
        IPs = []

        CFG.load()

        try:
            disk_usage = disk_free.split("\n")[1].split()

            #the following returns something like this: [['eno1', 'ec:b1:d7:66:2e:3a', '192.168.1.1'], ['enp0s20u12', '74:da:38:49:f8:2a', '155.198.232.206']]
            adapters_list = [ [i, netifaces.ifaddresses(i)[17][0]['addr'], netifaces.ifaddresses(i)[2][0]['addr']] for i in netifaces.interfaces() if 17 in netifaces.ifaddresses(i) and 2 in netifaces.ifaddresses(i) and netifaces.ifaddresses(i)[17][0]['addr'] != '00:00:00:00:00:00' ]
            for ad in adapters_list:
                CARDS [ ad[0] ] = {'MAC' : ad[1], 'IP' : ad[2]}
                IPs.append (ad[2])
            
           
            with os.popen('git rev-parse --abbrev-ref HEAD') as df:
                GIT_BRANCH = df.read() or "Not detected"
            #df = subprocess.Popen(['git', 'rev-parse', '--abbrev-ref', 'HEAD'], stdout=subprocess.PIPE)
            #GIT_BRANCH = df.communicate()[0].decode('utf-8')
            
            with os.popen('git status -s -uno') as df:
                NEEDS_UPDATE = df.read() != ""

            #df = subprocess.Popen(['git', 'status', '-s', '-uno'], stdout=subprocess.PIPE)
            #NEEDS_UPDATE = df.communicate()[0].decode('utf-8') != ""
            
            with os.popen('systemctl status picamera_gui.service') as df:
                try:
                    ACTIVE_SINCE = df.read().split("\n")[2] 
                except:
                    ACTIVE_SINCE = "Not running through systemd"

        except Exception as e:
            logging.error(e)

        return {'active_since': ACTIVE_SINCE, 'disk_usage': disk_usage, 'IPs' : IPs , 'CARDS': CARDS, 'GIT_BRANCH': GIT_BRANCH, 'NEEDS_UPDATE': NEEDS_UPDATE}
                
    elif req == 'time':
        return {'time': datetime.datetime.now().isoformat()}
        
    elif req == 'timestamp':
        return {'timestamp': datetime.datetime.now().timestamp() }
    
    elif req == 'log':
        with os.popen("journalctl -u picamera_gui -rb") as log:
            l = log.read()
        return {'log': l}
    
    # elif req == 'daemons':
    #     #returns active or inactive
    #     for daemon_name in SYSTEM_DAEMONS.keys():
        
    #         with os.popen("systemctl is-active %s" % daemon_name) as df:
    #             SYSTEM_DAEMONS[daemon_name]['active'] = df.read().strip()
    #     return SYSTEM_DAEMONS


    elif req == 'proxy':
        proxy = device_scanner._proxy
        if len(proxy.split("://")) == 1:
            proxy = "http://" + proxy
        return {"proxy": proxy }

    else:
        raise NotImplementedError()

@api.get('/devices')
@error_decorator
def devices():
    try:
        return device_scanner.get_all_devices_info()
    except Exception as e:
        logging.warning('device_scanner not defined or problem in _update_info')
        raise e


#Get the information of one device
@api.get('/device/<id>/data')
@warning_decorator
def get_device_info(id):
    device = device_scanner.get_device(id)
    
    # if we fail to access directly the device, we try the old info map
    if not device:
        try:
            return device_scanner.get_all_devices_info()[id]
        except:
            raise Exception("A device with ID %s is unknown to the system" % id)

    return device.info()

@api.post('/device/<id>/update')
@error_decorator
def device_update(id):

    post_data = bottle.request.body.read()
    device = device_scanner.get_device(id)
    return device.update(post_data)



@api.get('/device/<id>/picamera_data')
@error_decorator
def device_picamera(id):

    device = device_scanner.get_device(id)
    return device.picamera()
    
@api.get('/device/<id>/stream')
@error_decorator
def get_device_stream(id):
    '''
    Relay stream from an raspberry_pi to the client


    Method run in the node or proxy-connected device
    upon GETting from the client (node or proxy-connected device)
    Yields a frame in byte format
    obtained by doing a different GET request
    to a PiCamera server
    running on specific ip and port (probably an raspberry_pi)

    Used by the Angular controller
    to update the live stream
    '''

    device = device_scanner.get_device(id)
    bottle.response.set_header('Content-type', 'multipart/x-mixed-replace; boundary=frame')
    return device.relay_stream()


@api.post('/device/<id>/controls/<instruction>')
@error_decorator
def post_device_instructions(id, instruction):
    
    post_data = bottle.request.body.read()
    device = device_scanner.get_device(id)
    device.send_instruction(instruction, post_data)
    return get_device_info(id)

#endregion

@api.post('/controls/<id>/<action>')
def controls(id, action):
    '''
    Relay user data in the client to an raspberry_pi

    Method run in the node or proxy-connected device
    upon POSTting from the client (node or proxy-connected device)
    In turn, POSTs to an raspberry_pi
    The posted data is the result of initializing and urlencoding
    a ParameterSet object
    '''

    global control_info
    global machine_id
    if id != machine_id:
        raise WrongMachineID


    #print(f"Received post action: {action}")
    logging.warning(f'@node:device/{action} running...')

    # FUTURE: Uncomment this when actions admit data
    # data = bottle.request.json
    
    if action == "start":
        '''
        Method run in the raspberry_pi
        upon GETting from the client (node or proxy-connected device)
        Initialize the camera and start streaming on port 8008
        '''

        logging.warning('@raspberry_pi:device/start running...')
        global control
        global camera

        try:
            camera = picamera.PiCamera(resolution = (1280, 960), framerate = 2)

            def target():
                logging.info("Initializing CamStream server")
                server = CamStreamHTTPServer (camera, ('',8008), CamHandlerAnnotate)
                server.serve_forever()

            control = Thread(target = target)
            control.setDaemon(True)
            logging.info('Starting recording')
            control.start()
            
            resp = {'status': 'streaming'}
        except Exception:
            resp = {'status': 'failure'}

        logging.warning('@raspberry_pi:device/start done...')
        return resp

        
        
    elif action == "stop":
        '''
        Method run in the raspberry_pi
        upon GETting from the client (node or proxy-connected device)
        Stop recording
        '''
        logging.warning('@raspberry_pi:device/stop running...')

        try:
            camera.stop()
            logging.info('Stopping recording')
            resp = {'status': 'stopped'}
        except Exception:
            resp = {'status': 'failure'}
        
        logging.warning('@raspberry_pi:device/stop done...')
        
        return resp  
    


#endregion

# region only-devices

#region raspberry_pi-GET



@api.route('/static/<filepath:path>')
def server_static(filepath):
    # logging.warning(filepath)
    return bottle.static_file(filepath, root=STATIC_DIR)


@api.get('/data/<id>')
@error_decorator
def info(id):
    """
    This is information that is changing in time as the machine operates, such as FPS during tracking, CPU temperature etc
    """
    global control_info 
    global camera
    global machine_id
    
    if machine_id != id:
        raise WrongMachineID
    
    try:    
        control_info["current_timestamp"] = bottle.time.time()
        control_info["CPU_temp"] = get_core_temperature()
        control_info["loadavg"] = get_loadavg()
        control_info["fps"] = float(getattr(camera, "framerate", 0))
        return control_info
    except Exception as e:
        logging.error(e)
        return {"status": "offline", "experimental_info" : {}}



@api.get('/picamera_data/<id>')
@error_decorator
def picamera_data(id):
        '''
        Method run in the raspberry_pi
        upon GETting from the client (node or proxy-connected device)
        Returns a dictionary with camera attributes
        Used by the Angular controller to update the
        camera controls table
        '''

        global camera
        global param_set

        logging.warning('@raspberry_pi:/picamera_data running...')

        attributes = {}
        for k, v in param_set.params.items():
            try:
                v = getattr(camera, k, v.val)
                v = param_set._supported[k](v).val
                attributes[k] = v

            except Exception:
                logging.warning(f"attribute {k} is not available")
                continue

        logging.warning('@raspberry_pi:/picamera_data returns... :')
        logging.warning(attributes)

        return attributes


@api.get('/id')
@error_decorator
def name():

    global control_info
    return {"id": control_info["id"]}



#endregion

#region raspberry_pi-POST
@api.post('/update/<id>')
@error_decorator
def update(id):
    '''
    Update the parameters of the camera using the POSTed data

    Method run in the raspberry_pi
    Upon POSTing from the node
    Updates the camera parameters using the POSTed dictionary
    Data is formatted and validated using the ParameterSet class
    Returns True/False depending on the success of the camera update
    '''
    global control_info
    global picamera_info
    global camera
    global cache_set
    global machine_id
 

    if id != machine_id:
        raise WrongMachineID
    
    logging.warning('@raspberry_pi:/update running... ')

    try:
        data = bottle.request.body.read()
        if data is None: raise ValueError("botthe.request.body.read() does not work!")
    except ValueError:
        data = bottle.request.json

    try:
        if camera is None:
            logging.warning('Camera is not initialized')
            logging.warning('@raspberry_pi:/update done OK!')
            control_info.update({'status': 'stopped'})


        elif camera.closed:
            logging.warning('Camera is closed')
            logging.warning('@raspberry_pi:/update done OK!')
            control_info.update({'status': 'stopped'})

        elif type(data) is bytes and len(data) == 0:
            logging.warning('@raspberry_pi:/update_camera done unsuccessfully! :( ...')
            pass


        elif data is not None:
            try:
                logging.warning("data")
                logging.warning(data)
                param_set = ParameterSet({})
                logging.warning(param_set)
                param_set.restore_qs(data, encoding="utf-8")
                logging.warning("Restored")
                logging.warning(param_set)
                param_set.validate()
                param_set.cross_verify()
                logging.warning(param_set.params)
                param_set.pickle('param_set.pickle')
                pushed_set = param_set - cache_set 
    
                camera, attributes = pushed_set.update_cam(camera)
    
                cache_set = ParameterSet(attributes)
                cache_set.validate()
                cache_set.cross_verify()
                new_picamera_info = cache_set.as_dict()
                logging.warning(new_picamera_info)
                picamera_info.update(new_picamera_info)
                
                logging.warning('@raspberry_pi:/update_camera done successfully! :) ...')
                control_info.update({'status': 'streaming'})
            except Exception as e:
                logging.error(e)
                logging.error(traceback.print_exc())
                raise e

        else:
            pass

    except Exception as e:
        logging.error(e)
        logging.warning('@raspberry_pi:/update_camera done unsuccessfully! :(')
        control_info.update({'status': 'error'})

    finally:
        # return helpers.urlencode(control_info, encoding = "utf-8")
        return control_info


#endregion       

# endregion   

def close(exit_status=0):
    global control
    logging.warning(f"picamera_server.close() has run with exit_status: {exit_status}!")
    #if control is not None and control.is_alive():
    #    control.stop()
    #    control.join()
    #    control=None
    #else:
    #    control = None
    os._exit(exit_status)

#======================================================================================================================#
#############
### CLASSS TO BE REMOVED IF BOTTLE CHANGES TO 0.13
############
class CherootServer(bottle.ServerAdapter):
    def run(self, handler): # pragma: no cover
        from cheroot import wsgi
        from cheroot.ssl import builtin
        self.options['bind_addr'] = (self.host, self.port)
        self.options['wsgi_app'] = handler
        certfile = self.options.pop('certfile', None)
        keyfile = self.options.pop('keyfile', None)
        chainfile = self.options.pop('chainfile', None)
        server = wsgi.Server(**self.options)
        if certfile and keyfile:
            server.ssl_adapter = builtin.BuiltinSSLAdapter(
                    certfile, keyfile, chainfile)
        try:
            server.start()
        finally:
            server.stop()
#############

if __name__ == '__main__':


    time.sleep(1)
  
    if DEBUG:
        logging.basicConfig(level=logging.DEBUG)
        logging.info("Logging using DEBUG SETTINGS")

    try:
        #This checks if the patch has to be applied or not. We check if bottle has declared cherootserver
        #we assume that we are using cherrypy > 9
        from bottle import CherootServer
    except:
        #Trick bottle to think that cheroot is actulay cherrypy server, modifies the server_names allowed in bottle
        #so we use cheroot in background.
        SERVER="cherrypy"
        bottle.server_names["cherrypy"]=CherootServer(host='0.0.0.0', port=PORT)
        logging.warning("Cherrypy version is bigger than 9, we have to change to cheroot server")
        pass

    try:
        # default to a raspberry_pi
        import picamera
        from picamera_gui.ethoscope.src.ethoscope.web_utils.record import CamStreamHTTPServer
        from picamera_gui.utils.record import CamHandlerAnnotate
        from picamera_gui.ethoscope.src.ethoscope.web_utils.helpers import get_core_temperature, get_loadavg, get_machine_name, get_git_version, get_machine_id
        from zeroconf import ServiceInfo, Zeroconf
        from threading import Thread

        logging.warning("Detected server running on device")
        machine_id = get_machine_id()
        logging.warning(f"machine_id is {machine_id}")
        machine_name = get_machine_name()
        version = get_git_version()


        control_info = {"status": "stopped", "id": machine_id, "name": machine_name, "version": version, "experimental_info": {}}
        # compatibility with Ethoscope class
        control_info["experimental_info"].update({"location": None, "name": None, "run_id": None})
        picamera_info = {}
        camera = None

        hostname = socket.gethostname()
        uid = "%s-%s" % ( hostname, machine_id)
        address = socket.gethostbyname(hostname)

        serviceInfo = ServiceInfo("_picamera._tcp.local.",
            uid + "._picamera._tcp.local.",
            address = socket.inet_aton(address),
            port = PORT,
            properties = {
                'version': '0.0.1',
                'id_page': '/id',
                'static_page': '/static',
                'controls_page': '/controls',
                'user_options_page': '/user_options'
            } )
        zeroconf = Zeroconf()
        zeroconf.register_service(serviceInfo)
 
    except ImportError:
        # if they are not installed, we are on the node
        logging.warning("Detected server running on node")
        CFG = PiCameraConfiguration()
        PROXY = option_dict["proxy"] or CFG.content['proxy']['uri'] or None
        PROXY_PORT = option_dict["proxy_port"] or CFG.content['proxy']['port'] or None
        PROXY_REMOTE = CFG.content['proxy']['remote'] or None
        from picamera_gui.utils.device_scanner import PiCameraScanner
        device_scanner = PiCameraScanner(proxy=PROXY, remote_port=PROXY_REMOTE)
        try:
            device_scanner.start()
        except Exception as e:
            logging.error(e)
            raise e

    except Exception as e:
        logging.error(traceback.format_exc())
        raise e

    finally:
        try:
            bottle.run(api, host='0.0.0.0', port=PORT, debug=DEBUG, server=SERVER)
        except Exception as e:
            logging.error(traceback.format_exc())
            close(1)
            
        finally:
            close()

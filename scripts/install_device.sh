# Add picamera_gui to the Python path
echo "/opt/picamera_gui/" >> /usr/lib/python3.7/site-packages/easy-install.pth

# Create the systemd service
sudo cp systemd/picamera_gui_device.service /etc/systemd/system/
sudo systemctl daemon-reload
sudo systemctl start picamera_gui_device
sudo systemctl enable picamera_gui_device

# Make sure the ethoscope submodule is checked out
git submodule init
git submodule update
git submodule foreach git checkout picamera_gui

# Place default conf file
ln -s picamera_gui.conf /etc/picamera_gui.conf


(function(){

    // initialiwze the AngularJS application and call it picamera_gui
    var app = angular.module('picamera_gui', ['ngRoute', 'angularUtils.directives.dirPagination']);
    // this is reflected on the html tag in index.html
    // <html ng-app="picamera_gui">
    // if they don't match, the whole script is ignored!

    // def a router that connects controllers with templates
    app.config(function($routeProvider, $locationProvider) {
        $routeProvider

            // route for the home page
            .when('/', {
                templateUrl : '/static/pages/home.html',
                controller  : 'mainController'
            })

            // route for the sleep monitor page
            .when('/picamera/:device_id', {
                templateUrl : '/static/pages/picamera.html',
                controller  : 'picameraController'
            })

            // route for the help page
            /*.when('/help', {
                templateUrl : '/static/pages/help.html',
                controller  : 'helpController'
            })*/
        ;
        // use the HTML5 History API
        $locationProvider.html5Mode(true);
    });

    app.directive('input', function() {

        return {
          restrict: 'E',
          link: link 
        };

        function link(scope, elem, attrs) {
            elem.on('focus', function() {
                console.log(elem[0].name + ' has been focused!');
                scope.in_focus = elem[0].name;
            });
	    elem.on('blur', function() {
                console.log(elem[0].name + ' has been blurred!');
                scope.in_focus = undefined;
	    })
        }
    });

    app.directive('focusMe', function($timeout, $parse) {
	    return {
		    link: function(scope, element, attrs) {
		   
			    var model = $parse(attrs.focusMe);
			    scope.$watch(model, function(value) {
				    console.log('value=', value);
				    if(value === true) {
					    $timeout(function() {
						    element[0].focus()
					    });
				    }
			    });		    
		    }
	    }
    });


    // create the controller and inject Angular's $scope
    app.controller('mainController', function($scope, $http, $interval, $timeout, $log) {

        $http.get('/devices').success(function(data){
            $scope.devices = data;
        });
        var update_local_times = function(){
            $http.get('/node/time').success(function(data){
                t = new Date(data.time);
                $scope.time = t.toString();
                });
            var t = new Date();
            $scope.localtime = t.toString();
        };

        $scope.get_devices = function(){
            $http.get('/devices').success(function(data){

                data_list = [];

                for(d in data){
                    dev = data[d];
       	            if(dev.loadavg === undefined) {
                      dev.loadavg = ["NA","NA","NA"];
		              dev.color = "#add8e6";
           	        } else {
                       dev.loadavg = [Math.round(100 * dev.loadavg[0] / 4) + "%", Math.round(100 * dev.loadavg[1] / 4) + "%", Math.round(100 * dev.loadavg[2] / 4) + "%"];
                       dev.color = "#f44336";
           	        }
                    data_list.push(dev);
                }

                $scope.devices = data_list;
                $scope.n_devices=$scope.devices.length;
                status_summary = {};

                for(d in $scope.devices){
                    dev = $scope.devices[d];
                    if(!(dev.status in status_summary))
                    status_summary[dev.status] = 0;
                    status_summary[dev.status] += 1;
                }

                $scope.status_n_summary = status_summary
            })
        };
        
        $scope.secToDate = function(secs){
            d = new Date (isNaN(secs) ? secs : secs * 1000 );

            return d.toString();
        };

        $scope.elapsedtime = function(t){
            // Calculate the number of days left
            var days=Math.floor(t / 86400);
            // After deducting the days calculate the number of hours left
            var hours = Math.floor((t - (days * 86400 ))/3600)
            // After days and hours , how many minutes are left
            var minutes = Math.floor((t - (days * 86400 ) - (hours *3600 ))/60)
            // Finally how many seconds left after removing days, hours and minutes.
            var secs = Math.floor((t - (days * 86400 ) - (hours *3600 ) - (minutes*60)))

            if (days>0){
                var x =  days + " days, " + hours + "h, " + minutes + "min,  " + secs + "s ";
            } else if ( days==0 && hours>0){
                var x =   hours + "h, " + minutes + "min,  " + secs + "s ";
            } else if(days==0 && hours==0 && minutes>0){
                var x =  minutes + "min,  " + secs + "s ";
            } else if(days==0 && hours==0 && minutes==0 && secs > 0){
                var x =  secs + " s ";
            }
            return x;
        };
        
        $scope.$on('$viewContentLoaded',$scope.get_devices);


       var refresh_platform = function(){
            if (document.visibilityState=="visible"){
                $scope.get_devices();
                update_local_times();                   
                $log.info("Refresh");
            }
       };

       // refresh every 5 seconds
       refresh_data = $interval(refresh_platform, 1 * 1000);
        
        //clear interval when scope is destroyed
        $scope.$on("$destroy", function(){
            $interval.cancel(refresh_data);
            //clearInterval(refresh_data);
        });
    });
})()
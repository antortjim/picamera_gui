(function(){

    // initialiwze the AngularJS application and call it picamera_gui
    var app = angular.module('picamera_gui');
    // this is reflected on the html tag in index.html
    // <html ng-app="picamera_gui">
    // if they don't match, the whole script is ignored!

    
    app.directive('input', function() {

        return {
          restrict: 'E',
          link: link 
        };

        function link(scope, elem, attrs) {
            elem.on('focus', function() {
                console.log(elem[0].name + ' has been focused!');
                scope.in_focus = elem[0].name;
            });
	    elem.on('blur', function() {
                console.log(elem[0].name + ' has been blurred!');
                scope.in_focus = undefined;
	    })
        }
    });

    app.directive('focusMe', function($timeout, $parse) {
	    return {
		    link: function(scope, element, attrs) {
		   
			    var model = $parse(attrs.focusMe);
			    scope.$watch(model, function(value) {
                    if(value === true) {
					    $timeout(function() {
						    element[0].focus()
					    });
				    }
			    
			    });

			    //
		    
		    }
	    }
    })


    // create the controller and inject Angular's $scope
    app.controller('picameraController', function($scope, $http, $routeParams, $interval, $timeout, $log) {

        device_id = $routeParams.device_id;

        $scope.input_invalid = true;
        $scope.input = {};
        $scope.input_cache = {};
        $scope.output = {};
        $scope.init = true;

        $http.get('/device/'+ device_id+'/data').success(function(data){
            $scope.device = data;
            $log.info("Name of device");
            $log.info($scope.device['name']);
            $scope.isActive = ( $scope.device['name'].split("_").pop() != "000" );
        });

        
        $scope.remote_pages = {
            "update": `/device/${device_id}/update`, "picamera": `/device/${device_id}/picamera_data`,
            "stream": `/device/${device_id}/stream`}

        $scope.refresh_stream = function(device_id) {
            $scope.stream_url = $scope.remote_pages["stream"] + "?" + Date.now() ;
        }

        $scope.picamera = {}; // to control the device


        $scope.picamera.start_date_time = function(unix_timestamp){
            var date = new Date(unix_timestamp*1000);
            return date.toUTCString();
        };
        $scope.picamera.elapsedtime = function(t){
            // Calculate the number of days left
            var days=Math.floor(t / 86400);
            // After deducting the days calculate the number of hours left
            var hours = Math.floor((t - (days * 86400 ))/3600)
            // After days and hours , how many minutes are left
            var minutes = Math.floor((t - (days * 86400 ) - (hours *3600 ))/60)
            // Finally how many seconds left after removing days, hours and minutes.
            var secs = Math.floor((t - (days * 86400 ) - (hours *3600 ) - (minutes*60)))

            if (days>0){
                var x =  days + " days, " + hours + "h, " + minutes + "min,  " + secs + "s ";
            }else if ( days==0 && hours>0){
                var x =   hours + "h, " + minutes + "min,  " + secs + "s ";
            }else if(days==0 && hours==0 && minutes>0){
                var x =  minutes + "min,  " + secs + "s ";
            }else if(days==0 && hours==0 && minutes==0 && secs > 0){
                var x =  secs + " s ";
            }
            return x;

        };


        // def get_attributes
        // GET the method on /device/get_attributes
        // and save the result to the attributes array 
        $scope.get_attributes = function (device_id) {
            remote_url = $scope.remote_pages["picamera"]
            
            $log.info(remote_url);
            $http.get(remote_url).success(function(data) {
                $log.info('$scope.get_attributes has fetched these data...');
                $log.info(data);

                // format the data into an array of dicts with entries Key and Value
                for (var key in data) {
                    // check if the property/key is defined in the object itself, not in parent
                    if (data.hasOwnProperty(key)) {           
                        // console.log(key, data[key]);
                        req1 = $scope.in_focus != key;
                        req2 = true;
                
                        for (var attr_key in $scope.output) {
                            // $scope.attributes.forEach(function(value,index,aray) {
                            if(attr_key == key) {
                            // $log.info(data[key]);
                            // $log.info(value.Value);
                                req2 = data[key] != $scope.output[attr_key];
                            };
                        };

                        if (req1 & req2) {
                            $scope.output[key] = data[key];
                            
                            if ($scope.init) {
                                $scope.input[key] = data[key];
                            }
                            $log.info('$scope.get_attributes has processed these data')
                            $log.info($scope.output);
                        }
                    }
                    
                };
                $scope.init = false;

                // assign this array to the scope
                // $scope.attributes = attributes;
                $log.info('$scope.get_attributes DONE...');
                
            });
        };

        // run get_attributes once upon initialization
        $scope.get_attributes();


        // define a global variable
        // post_attributes_start
        // that will save the time of the last call
        // to post_attributes
        var post_attributes_start;
        var last_attributes_end;
        // def post_attributes
        // POST the attributes obtained in get_attributes
        // to /device/post_attributes
        $scope.post_attributes = function(device_id) {


	    $log.info("post_attributes called");

            // update the value in post_attributes_start
            // this update happens is propagated to all calls to post_attributes
            // that are still waiting due to the timeout!!!!!!!
            // this is achieved thanks to post_attributes_start being global
            post_attributes_start  = Date.now();
                           
            // define a function that waits for 1 seconds
            delay_ms = 1 * 1000;
                
            function stateChange(post_attributes_start) {
                
                if ($scope.input_invalid) {
                    
                    // set local_start to post_attributes_start
                    local_start = post_attributes_start;
                    
                    // wait delay_ms ms
                    setTimeout(function () {
                        // after waiting for delay_ms ms,
                        // is post_attributes_start the same as local_start?
                        // if it is, it was not invalidated
                        // by a newer call to post_attributes
                        if (post_attributes_start == local_start) {
                        // if (true) {
                            $log.info('Running $scope.post_attributes...')
                            // parameters = {}
                            // 
                            // // shape attributes in a format that the server expects
                            // // instead of an array of dictionaries with pairs Key and Value
                            // // it will be a single dictionary where the keys are the Keys in each
                            // // pair of the original array, and the values, the Values.
                            // for(i=0; i < $scope.input.length; i++) {
                            //     // get the ith pair
                            //     pair = $scope.input[i];
                            //     // add an entry to the attributes dictionary
                            //     // with key pair.Key and value pair.Value
                            //     parameters[pair.Key] = pair.Value;
                            // }
                
                            // post the resulting dictionary to the server
                            $log.info('$scope.input');
                            $log.info($scope.input);
                            // $log.info($scope.remote_pages["update"]);

                            $http.post($scope.remote_pages["update"], data = $scope.input).success(function(data) {
                                $scope.status = data['status'];
                                $log.info('camera update status: ' + $scope.status);
                            });

                            $scope.input_invalid = false;
                            delay_2 = 3 * 1000
                            setTimeout($scope.get_attributes, delay_2);
			                $scope.input_invalid = true;
                            $log.info('$scope.post_attributes DONE...');
                        
                        // otherwise, it is invalidated and we should do nothing!
                        } else {
			                $log.debug("post_attributes called at " + Date(local_start).toString()  + " got invalidated because a new call happened less than " + delay_ms + "ms after");
                        }
                    }, delay_ms);
                } else {
                    $log.debug("invalidated becuase get_attributes was called less than " + delay_2 + " ms after");
                }
            }

            // actually run the timedOut function
            stateChange(post_attributes_start)
        };

        // def check_attributes
        // check_attributes is a function that declares a watcher
        // it checks for changes in the camera parameters table
        // which can be changed by the user
        // the state of the table is captured by
        
        // scope$attributes thus watching $scope.attributes
        // is the as watching the table!
        $scope.check_attributes = function(){
	
            // make sure $scope.attributes is defined
            // it may not be defined if the call to get_attributes
            // above is not done yet!
            condition = angular.isDefined($scope.input);

            if(condition){
                // runs when $scope.attributes is defined

                // watch the attributes array for changes
                // everytime it changes, run post_attributes
                // to update the attributes on the server
                $scope.$watch(
                    // what to watch i.e. $scope.attributes
                    // we dont have to type $scope., just attributes
                    'input', 
                    // what do to upon changes
                    $scope.post_attributes,
                    // true means: look at all the elements in the array one-by-one
                    // i.e. do a deep comparison
                    true);
            } else {
                // check again in a second
                setTimeout($scope.check_attributes, 1000);
            }
        };
        // create the timeout-watcher 
        $scope.check_attributes();
    
        // link the start button to the corresponding method in the server
        $scope.start = function(device_id) {
            $http.post(`/device/${device_id}/controls/start`);
        };

        // link the stop button to the corresponding method in the server
        $scope.stop = function(device_id) {
            $http.post(`/device/${device_id}/controls/stop`);
        };

        $scope.pause = function(device_id) {};


        $scope.node_datetime = "Node Time"
        $scope.device_datetime = "Device Time"

        // run refresh platform every refresh_freq_s
        // if the browser tab where the app is running
        // is visible on the screen
        var refresh_freq_s = 2;
        refresh_platform = function(){
            if (document.visibilityState=="visible"){
            $scope.refresh_stream();
            $log.info($scope.input);

            }
        };
        refresh_data = $interval(refresh_platform, refresh_freq_s * 1000);
            
        //clear interval when scope is destroyed
        $scope.$on("$destroy", function(){
            $interval.cancel(refresh_data);
            clearInterval(refresh_data);
        });

    // end of controller
    });
})()
